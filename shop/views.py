from django.shortcuts import render

data = [
    {"name": "phon1", "price": 100, "img": "images/1.png", "stars": "****"},
    {"name": "phon2", "price": 125, "img": "images/2.png", "stars": "***"},
    {"name": "phon3", "price": 130, "img": "images/3.png", "stars": "**"},
    {"name": "phon4", "price": 1, "img": "images/4.png", "stars": "*"},
    {"name": "phon5", "price": 50, "img": "images/5.png", "stars": "**"},
    {"name": "phon6", "price": 10000000, "img": "images/6.png", "stars": "****"},
]


# Create your views here.
def ShopView(reqest):
    return render(reqest, "shop/shop_page.html", {"phons": data})
